; Copyright 2003 Tematic Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
        AREA    FlashProg, CODE, READONLY

p555    RN      4
p2AA    RN      5
c55     RN      6
cAA     RN      7
DQ5     *       &20
DQ7     *       &80
V_bit   *       1:SHL:28

        MOV     R0, #&C0000000
        ADD     R0, R0, #128*1024*1024
        SUB     R0, R0, #4*1024*1024    ; &C7C00000
        ADD     R1, R0, #4*1024*1024    ; Image size
        MOV     R3, #&00000000
        ADD     p555, R3, #&055*2
        ADD     p555, p555, #&500*2     ; R4 -> &555
        ADD     p2AA, R3, #&0AA*2
        ADD     p2AA, p2AA, #&200*2     ; R5 -> &2AA
        MOV     c55, #&55               ; R6 = &55
        MOV     cAA, #&AA               ; R7 = &AA
        ; Erase
        STRH    cAA, [p555]
        STRH    c55, [p2AA]
        MOV     R8, #&80
        STRH    R8, [p555]
        STRH    cAA, [p555]
        STRH    c55, [p2AA]
        MOV     R8, #&10
        STRH    R8, [p555]
        ; Poll for completion
        MOV     R8, #&FF
        ORR     R8, R8, #&FF00
        BL      Poll
        BVS     EraseError
        ; Program
        SUB     R3, R3, #2
ProgramLoop
        STRH    cAA, [p555]
        STRH    c55, [p2AA]
        MOV     R8, #&A0
        STRH    R8, [p555]
        LDRH    R8, [R0], #2
        STRH    R8, [R3, #2]!
        BL      Poll
        BVS     ProgramError
        TEQ     R0, R1
        BNE     ProgramLoop

Success
        BKPT    &600D
        B       Stop
ProgramError
        BKPT    &BADF
        B       Stop
EraseError
        BKPT    &BADE
        B       Stop

Stop    B       Stop


Poll    ROUT
        AND     R10, R8, #DQ7
10      LDRH    R9, [R3]
        AND     R11, R9, #DQ7
        CMP     R11, R10        ; Clear V
        BEQ     Done
        TST     R9, #DQ5
        BEQ     %BT10
Done    LDRH    R9, [R3]
        TEQ     R9, R8
        MOVEQ   PC,R14
        ; Read/Reset
        MOV     R10, #&F0
        STRH    R10, [R3]
        MSR     CPSR_f, #V_bit
        MOV     PC, R14

        END
